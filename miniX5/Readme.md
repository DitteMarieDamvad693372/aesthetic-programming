## MINIX 5 - A generative program

[Click here to run my MiniX5](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX5/)


[Click here for my code](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX5/sketch.js)

![Picture of my program](/miniX5/miniX5.png "Picture of my program")

## Creating the rules 

This week’s minix project with auto-generative art was a bit challenging project because of yhe fact that you had to come up with rules to implement into a program, before actually coding anything. I knew I wanted to implement the random syntax into my color scheme, and that I wanted to make something very colorfull to look at. 

Eventually I chose the following rules.
1.	There should be drawn rectangles in random places and sizes throughout the canvas.
2.	The rectangles should change colors in some areas ruled by coordinates from the x-axis or the y-axis.

## My program

My final program consists exclusively of loops of rectangles. The program starts with a black canvas and when the program starts the rectangles are drawn immediately in different colors with different colors of strokes (outlines). 
My rules produce emergent behavior because of the randomness of the placement of rectangles, level of transparency as well as the switching colors. I feel like my program looks like an abstract painting and it can therefore be interpreted in many ways, depending on who you are. Prior to writing the code, I thought that the outcome would be very cheerfull and fun to look at, but I discovered that it actually depends on what colors are generated. If for example the more dull colors were generated at the same time, the program looks more gloomy and would therefore have a different meaning. This was not something I had thought of before making the program but discovered when I ran it a couple of times and obeserved the outcome. 

The main syntax I have worked with in this minix is the random() function. I used the random function to almost everything. I used it to make rectangles appear in random places, sizes and in random colors with random strokes throughout the canvas. I used if statements to make the different colored rectangles appear in three horizontal sections on the y-axis. I then used a for loop to draw loops of rectangles continuously when the program is run. The alpha paramether was also used to draw the rectangles in different levels of transparency. It kept it simple for this miniX, and I am actually somewhat happy with the result. I think it was fun to think of rules before writing the code, event though it was a bit difficult. This miniX I really focused on the low floors part of loow floors, high ceiling. 



## Auto generation

As a result of this minix, I was reminded of the importance of randomness in generative art. It affects both aesthetics and technical relations. 
Randomness adds an element of surprise and unpredictability to projects. Because my rules are so simple, this program could have looked very different depending on aesthetic preferences and technical choices.
It was really cool to work with the 10 prints source code, because 10 print can be regarded as the first creative program using machines to interpret instructions from humans resulting in a creative, random and generative maze pattern.
Ten prints introduced algorithmic logic derived from mathematical machines as a creative and artistic tool. These were the first examples of generative art.


>>>
“Generative art refers to any art practice where artists use a system, such as a set of natural languages, rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art.”


(definition of generative art, Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 126)
>>>


It is interesting how randomness in programming can lead to reinterpreting how we see and experience art. 
Similarly to classical art, Generative Art is a platform from which to create abstract art.
The creation of auto-generators opens up new creative perspectives because they can simulate complex movements and living life. 
Ultimately, the versatility of generative programming and the addition of randomness and automation are what make generative art so appealing.

