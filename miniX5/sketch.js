
function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  frameRate(10);
  }


function draw() {

  noStroke();
  let randomYlocation = random(windowHeight);

//make random sizes of rectangles in random colors
//only when y < 213
  if (randomYlocation < 214) {
    stroke(random(255),random(255),random(255));
    fill(random(255), random(255), random(2555),random(255,230));
  }

  //make random sizes of rectangles in random colors
  //draws rectangles on the y-axis between 220-500
  if(randomYlocation > 220 && randomYlocation < 500){
      stroke(random(255),random(255),random(255));
			fill(random(255), random(255), random(255),random(255,230));
	}

  //make random sizes of rectangles in random colors
  //when y > 400
	if(randomYlocation > 400){
      stroke(random(255),random(255),random(255));
		  fill(random(255), random(255), random(255),random(255,230));
  }

//set the rect size
  let randomdiameter = random(2);

//drawing/looping the rectangles
  for (let i = 0; i < 10 ; i++) {
    rect(random(windowWidth+80),randomYlocation, randomdiameter+80, randomdiameter+80);
  }

//draw loops of random color rectangles on the entire canvas
  for (let j = 0; j < 10; j++) {
    stroke(random(255),random(255),random(255));
    fill(random(255),random(255),random(255));
    rect(random(windowWidth),random(windowHeight),randomdiameter+80,randomdiameter+20);
  }
  }

    fill(135, 206, 235,random(300));
