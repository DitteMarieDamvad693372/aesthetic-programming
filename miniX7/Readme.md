## MiniX7 Revisit the past


[Click here to run my MiniX7](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX7/)


[Click here for my code](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX7/sketch.js)




![Picture of my program](/miniX3/throbber.gif "Picture of my program")


![Picture of my program](/miniX7/throbber2.gif "Picture of my program")

## My Program

When you first open my program you are met with a starry univers and in the center there is a sun, with one undefines planet in its orbit. When you move your mouse infront of the canvas small stars follow the path of it. If the mouse is pressed another planet will spawn from the x and y location of the mousepress and fly into orbit with the other planet circulating the sun. Everytime the mouse is pressed another planet will spawn into the orbit, eventually creating your own little solar system. Under the sun the text "Loading world.." is written with promises of something more coming the users way. 

## Reworked Throbber

For my seventh MiniX I have decided to improve my throbber from MiniX 4. When I made the original, there were not much thought put into the look of the program, but rather the contextual meaning of time passing, and even that felt a bit too 'thin'. The first throbber showed the sun and moon circulating the earth. I know that this scientifically is not correct, but I wanted to somehow showcase how us (humans) think the whole world revolves around us. I liked this idea, but was eventually really agitated at the fact that it wasn't scientifically correct. Therefore, I have reworked my throbber so random undefined planets circulate around the sun. Not only is this correct, but waaay more pleasing to look at. So, the idea of a solar system "throbber" is still present, but the look and feel of the program is different. I changed the background from a blue sky with clouds to a starry interacting univers as it was more natural and a more fun alternative to the static background. 
 
## New syntax

When I first looked at how to change the throbber for the better, I knew I wanted to incoorporate new syntax that I have learned since making the third miniX. I wanted to incoorporate what we learned last week with object abstraction, more specifaclly classes of objects. I therefore created a class for the Planets, and also played with the random function quite a lot. This MiniX I have definetly learned more about how class works and also how to use const instead of for example let (Like a variable created with let, a constant that is created with const is a container for a value, however constants cannot be reassigned once they are declared. ).


## What is the relation between aesthetic programming and digital culture? 
As digital culture becomes more and more part of everyday life, (aesthetic) programming has also begun to dominate the digital art scene.Programming and coding are fundamental to most people's daily lives. Experiences are more mobile with programming. You can take them virtually anywhere with you on any digital device. Each day we use some form of technical device, and these devices are composed of some hardware and, more importantly, of software. In the digital age, we use a lot of programmed things, apps, programs, webpages, etc. every day, so it is important for us to understand how these things are made and how some aren't exactly neutral. For that reason, I believe we need to understand programming, who writes it, how it is written, and how it affects our digital life and culture.




## How does my work demonstrate the perspective of aesthetic programming?

I would like to bring forth this quote from my miniX3 :

>>>

When encountering a throbber in everyday digital culture, e.g. when streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction I think a throbber communicates a waiting time that we know is'nt too long, as we have learned through the years of encountering it, that we just have to wait a bit at least at most times. It also really doesnt communicate anything other than a problem has occurred or something is or needs to be loaded. I would argue that my throbber does the same, but implies that a lot more time is passing, and as I said kind of provokes a thougt process of exactly HOW much time you spend. With the text under the earth saying "LOADING WORLD..." I also wanted to create a thought process of what is happening in the world, while you are waiting at the loading screen.

>>>

I would argue that this quote is an example on how my work adress the cultural and aesthetic dimensions of programming as a means to think and act critically. Throughout all my my minix's (except maybe the previous) I have always thought critically and provocative in terms of our culture and everchanging society. I have included political aestethics in the form of things like topics of gender and in this minix, the discussion on how much time we spend on the internet and how it influences us. 

>>>

Political aesthetics refers back to the critical theory of the Frankfurt School, particularly to the ideas of Theodor Adorno and Walter Benjamin, that enforce the concept that cultural production — which would now naturally include programming — must be seen in a social
context. Understood in this way programming becomes a kind of “force-field” with which to understand material conditions and social contradictions, just as the interpretation of art once operated “as a kind of code language for processes taking place within society.”

>>>

This quote from the book also highlights how programming becomes a tool to translate critical thinking into something artistic and meaningfull, and I really agree with this. Learning how to code has for sure given me a medium to translate my critical thoughts in a way that bot corresponds to the present computational age but also exceeds in a way that equips me to be able to use it in a larger scale in the future.


## References

[Stars P5.js](https://editor.p5js.org/ag3439/sketches/Skgh1ZQtQ)

[Planets P5.js](https://happycoding.io/examples/p5js/creating-classes/planets)

[Const P5.js Reference](https://p5js.org/reference/#/p5/const) 
