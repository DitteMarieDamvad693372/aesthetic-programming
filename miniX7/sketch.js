const planets = []; // constants will stay the same whereas variables can change throuhout the program

function setup() {
  createCanvas(windowWidth, windowHeight);


  planets.push(new Planet(width * 0.75, height * 0.5));
}

function mousePressed() {
  planets.push(new Planet(mouseX, mouseY));
}

// Drawing the stars
function draw() {
   background(0,0,35,25);

   var galaxy = {
   locationX : random(width),
   locationY : random(height),
   size : random(1,6)
 }

 ellipse(mouseX ,mouseY, galaxy.size, galaxy.size); // the stars are drawn
  ellipse(galaxy.locationX ,galaxy.locationY, galaxy.size, galaxy.size);

  //  drawing the sun
  noStroke()
  fill(255, 255, 0);
  circle(width / 2, height / 2, 140);

  for (const planet of planets) {
    planet.draw();
  }
}

class Planet {
  constructor(x, y) { // istedetfor at refere til det som planet defineres det som X,Y
    this.x = x;
    this.y = y;

    // random size
    this.size = random(10, 50);

    // random speed
    this.deltaX = random(-10, 10);
    this.deltaY = random(-10, 10);

    // random color
    this.c = color(random(160, 230), random(160, 190), random(178, 102));
  }

  draw() {
    const sunX = width / 2;
    const sunY = height / 2;
    const distanceFromSun = dist(this.x, this.y, sunX, sunY);

    // planets accelerate faster when they're closer to the sun
    // this simulates gravity pulling them in faster and faster
    this.deltaX += (sunX - this.x) / distanceFromSun;
    this.deltaY += (sunY - this.y) / distanceFromSun;

    this.x += this.deltaX;
    this.y += this.deltaY;

    fill(this.c);
    ellipse(this.x, this.y, this.size);

    fill(78,77,70)
    textSize(25)
    textStyle(BOLD)
    text("LOADING WORLD ...",605,570)

    // Main text
    fill(255)
    textSize(25)
    textStyle(BOLD)
    text("LOADING WORLD ...",602,570)

  }
}
