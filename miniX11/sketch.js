let reasons;
let adSound;
let advert;
let googleI;
let clo;
let scroll;
let nameInput;
let testButton;
let greeting;
let quota;

//Pictures, sound effect and the JSON File
function preload(){
  data = loadJSON("reasons.json")
  google = loadImage("googleHomepage.png");
  ad = loadImage("satanisk.png");
  back = loadImage("back.png")
  googleI = loadImage("GoogleI.png")
  adSound = loadSound("adsound.mp3")
  bottomScreen = loadImage("bottom.png");
  blurBack = loadImage("googleHomepage1.png");
  avatar = loadImage("avatar.png");
  quota = loadFont("Quota-Regular.otf");

}

function setup(){
  createCanvas(windowWidth, windowHeight);
  background(back);

//The Google icon, when clicked you get into our google webbrowser
  googleI = createImg("GoogleI.png")
  googleI.size(width/19.5,height/14.6)
  googleI.position(width/2.17, height/1.105)
//When mouse is pressed the function googleScreen is shown
  googleI.mousePressed(chromeLogIn);

//text-box for log in
  nameInput = createInput();
  nameInput.position(width/2.45, height/1.8);
  nameInput.size(250,50);
  nameInput.style('display', 'none');
  nameInput.style('font-size', '25px');

//button for logging in
  push()
  logInButton = createButton('SIGN IN');
  logInButton.position(width/2.22,height/1.6);
  logInButton.style("background","#72009B")
  logInButton.style("color", "#ffffff");
  logInButton.size(150, 40);
  logInButton.style('font-size', '20px');
  logInButton.style('display', 'none');
  logInButton.mousePressed(googleScreen);
  pop()

// The 'why am i seeing this' button
  advert = createButton("Why am I seeing this?");
  advert.style("background", "#c8c8c8");
  advert.style("color", "#ffffff");
  advert.size(width/8, height/32);
  advert.position(width/1.62, height/2.35);
  advert.mousePressed(theAlgorithm);
  advert.style('display', 'none');

// The red "X" button on the window
  clo = createButton("X")
  clo.style("background", "#ed1118");
  clo.style("color", "#ffffff");
  clo.size(width/45, height/35);
  clo.position(width/1.28, height/8);
  clo.mousePressed();
  clo.style('display', 'none');

// The scroll bar
  scroll = createSlider(1, 45, 1);
  scroll.position(width/1.67, height/2);
  scroll.style("transform","rotate(90deg)");
  scroll.style("width","550px");
  scroll.mouseReleased(scrollBar);
  scroll.style('display', 'none');

}
//makes it possible to use enter to log in.
function keyPressed() {
  if (keyCode === 13) {
    googleScreen();
  }
}

function chromeLogIn() {
  googleI.hide();

  push();
  imageMode(CENTER);
  image(blurBack, width/2, height/2, width/1.18, height/1.1);
  pop();

  push();
  imageMode(CENTER);
  image(avatar,width/2, height/2.7, width/8.5, height/4.7);
  pop();

  push();
  textAlign(CENTER)
  fill(10);
  textSize(30);
  text("TYPE IN YOUR NAME" ,width/2, height/1.95);
  textSize(20);
  text("TO LOG IN TO YOUR GOOGLE ACCOUNT",width/2, height/1.85);
  pop();

  logInButton.show();
  nameInput.show();

}

function googleScreen() {
  nameInput.hide();
  logInButton.hide();

// The google screen
  push();
  imageMode(CENTER);
  image(google, width/2, height/2, width/1.18, height/1.1);
  pop();

// Sound effect for the ad
  adSound.play();

// Making the ad appear with a 1.5 second delay
  adShower = setInterval(adShower, 1500);
}

function adShower(){
  push();
  rectMode(CENTER);
  fill(200);
  stroke(140);
  rect(width/2, height/1.46, width/2, height/1.92,10);

  imageMode(CENTER);
  image(ad, width/2, height/1.44, width/2-20, height/2-20);
  pop();

  noStroke();
  fill(255);
  textSize(15);
  text("Advertisement", width/2.15, height/2.24)
  advert.show();
}

function theAlgorithm(){
//clearInterval stops the ad from printing every 1.5 seconds:)
  clearInterval(adShower);
  advert.hide();
//this the input you get at first when logging in (the name).
  name = nameInput.value();

  push();
  rectMode(CENTER);
  fill(10);
  stroke(140);
  rect(width/2, height/2, width/1.6, height/1.3,10);
  pop();

  fill(0,255,0);

  reasons = data.reasonsWhen;
  reasons1 = data.reasonsWhat;

    push();
    let i = 0;
    for (let t = 0; t <= height/1.46; t++) {
      textFont(quota);
      text(reasons[i], width/4.6, height/5+t);
      textFont("Courier New");
      text("You" + reasons1[i], width/2.5, height/5+t);
      t += 25
      i += 1
   }

  pop();
  push();
  fill(50);
  rectMode(CENTER);
  rect(width/2.1, height/6.6, width/1.8, height/20,10)
  fill(225);
  noStroke();
  textSize(40);
  text("Data log for " + name + ":", width/4.6,height/6);
  pop();

  scroll.show();
  clo.show()

   push();
   imageMode(CENTER);
   image(bottomScreen,width/2,height/1.075,width,height/5.3);
    pop();

}

function scrollBar() {
  push();
  fill(10);
  rectMode(CENTER);
  rect(width/2, height/1.85, width/1.7, height/1.4,10);
  pop();

  textFont("Courier New");
  fill(0,255,0);

  for (let u = 0; u <= height/1.46; u++){
    for(let scl=0; scl<scroll.value();scl++){
      textFont(quota);
      text(random(reasons),width/4.6, height/5+u+scl);
      textFont("Courier New");
      text("You" + random(reasons1), width/2.5, height/5+u+scl)
      u += 25
    }
  }

  push();
  imageMode(CENTER);
  image(bottomScreen,width/2,height/1.075,width,height/5.3);
  pop();

}
