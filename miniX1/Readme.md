# MiniX 1 

[Click here to run my MiniX1](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX1/)


[Click here for my code](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX1/sketch.js)



_Below is a screenshot of my MiniX_
![Picture of my program](/miniX1/Minix1.png "Picture of my program")


## My MiniX 1
For my first MiniX, I decided to make something that illustrated my first programming expereince. Quite plainly, it is a rotating Torus, with a rotating text that spells "help" in the middle. I used my favourite colors for the background and the color of the elements, blue and orange. 
This illustrates my first experience as it was both fun and frustrating. The helplessness I felt when something didnt work, combined with my initial final code which I would deem somewhat of an Aesthetic animation, resultet in my upload. The kind of comical way the word "help" just floats around, is exacatly how I felt coding it. Despite the rough paths I learned that everything works out in the end, if I just put enough effort and patience in my work. 

I started exploring the p5.js reference list, and initially wanted to look at how to add text. I found a rotating text, which i decided to use, and straight up copied it into Atom. I quickly realised that the text function required a loaded font, which I had not done. Through my Mac's "Fontbook" application, I was able to download the ORCRASdotf font, which i then saved in the folder that my Sketch.js was in. I learned to use the 'let' function, in order to **define** the font to simplify the code. This was proven quite usefull for me. Next I created my canvas, the text size and  where it should be placed. Then with the draw function, I sued the 'let' function again, and defined 'millis' as 'time', 'millis' being the fucntion used to return the number of milliseconds from starting the program. With that I could create the rotation time on the X and Y axis. Lastly, I wrote my text and its coordinates. 
I wanted to explore the 3D functions aswell, so I decided to add rotating Torus to my code. Here I also copied it from the reference and altered it. Here I expereinced that I had to rearrange some lines of code in order for it to work, and not rule eachother out. To be honest, I just played around with different placements untill it worked. I ended up placing the codes for the Torus before the function draw section of my rotating text. With the torus code, I first determined the speed of the rotation with 'framecount' which contains the number of frames that have been displayed since the program started. Then I had my torus fucntion and used 'fill' to color it. Because of the placement of this code, the 'fill' also affected my text, and made that orange aswell. This I want to change in the future by learning how to 'seperate' code, so it doesnt affect every line.
To conclude, I copied the torus() and text() function, but altered it to fit my preferences. I think it was a great way to start, as I could look at examples and read about the functions. 

## My First Programming Experience
As i mentioned in class, I have never written code before. I was a bit scared of it, as I am not a very patient person, but it reallly wasn't as bad as I had imagined. I ofcourse had some problems, and sometimes my code didnt work, but it worked out as I checked the p5.js reference's and read the descriptions of the codes. For me, this was the best help for undertstanding what i wrote. I also discovered that things are simple and literal which makes it easy understanding the different possible functions. 

Reading and writing code are different in my experience since reading code allows you to discover new possibilities of creations whereas writing code allows you to express yourself. Though they go together in the sense that after reading code I can learn to write it, and after writing it I am better equipped to understand reading new code, which I especially felt after a little while, exploring the references and trying to code a few things.

Generally, I think of programming as "the new literacy" and "the second literacy" as Anette Vee mentions. The concept of programming is being able to control, design, and express technology. But, I also think that programming is understanding the technology infrastructure of the digital age logically. 
Additionally, I was both intrigued and troubled by Anette Vee's notion of coding as mass literacy. It is interesting to me because conceptually, programming writing differs fundamentally from traditional writing. 
On a political level, I find Anette Vee's notion troubling as she mentions how literacy is a social construction that changes over time, rendering the non-coders illiterate. When I first read this I thought that it was a bit harsh, and that our society simply wouldn't come to that point. But after thinking about for a while, I realized that she might be right in her assumptions. In our society knowlegde is power and if knowlegde consists of knowing how to read and write code, which it has become, I geniuenly think that in the future we might view non-coders as more 'stupid'. I will back this up with adding how accessible and prioritised code has become and continues to become. It is so easily attainable, and in most cases free, that we at some point does not have an excuse to not know how to read and write basic code. 

## References

[P5.js Text()](https://p5js.org/reference/#/p5/text)

[P5.js torus()](https://p5js.org/reference/#/p5/torus)

