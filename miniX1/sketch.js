  function preload() {
  OCRAStd = loadFont('OCRAStdotf');
  }
  function setup() { //code within this function will only be “run once” by the sketch work file.
    createCanvas(1500, 820, WEBGL); //WEBGL enables 3d render, creating a third dimension Z.
     textFont(OCRAStd);
    // textFont(ORCRAStd);
    textSize(width / 20);  
    textAlign(CENTER, BASELINE); 
    //placement of the text
  }
  function draw() { // Taking cues from drawing practice in visual arts, code within this function will keep on looping, and that means function draw() is called on for each running frame. The default rate is 60 frames/times per second, and this is especially useful when things are set in motion or constantly being captured
    background(18,0,255);

  rotateX(frameCount * 0.01);
  rotateY(frameCount * 0.01);
  //rotation of object in X and Y direction
  //The system variable frameCount contains the number of frames that have been displayed since the program started. 
  torus(100, 10);
  fill(250,157,1)

    let time = millis();
    //timing how long since program started
    //defining function=millis as time for ease of use
    //millis() is a time-related syntax and returns the number of milliseconds since the program started which makes it similar to frameCount().
    rotateX(time / 1100);
    rotateZ(time / 1200);
    text('help', 0, 0);
  }
