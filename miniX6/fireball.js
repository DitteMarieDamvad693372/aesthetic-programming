class Shots {
    constructor(){ //initalize the objects
      this.x = 0;
      this.y = 0;
      this.xspeed = 1;
      this.yspeed = 0;
      this.total = 0;
      this.tail =[];
   }

dir(x,y) { //direction
 this.xspeed = x;
 this.yspeed = y;
}

pick(pos){ //pick up bottles/shots
  let d = dist(this.x,this.y,pos.x,pos.y);
  if (d<2){
    this.total++;
    return true;
  }else{
    return false;
  }
}

  death(){ //when the party crashes and you "loose" the shots
    for (let i=0;i<this.tail.length;i++){
      let pos = this.tail[i];
      let d = dist(this.x,this.y,pos.x,pos.y);
      if(d<1){ //when the distance is less than 1 then you have crashed and are now alone agian
        this.total = 0;
        this.tail=[];
      }
    }
  }


  update() { //how pitbull move around with the shots
    for (let i = 0; i < this.tail.length - 1; i++) {
      this.tail[i] = this.tail[i + 1];
    }
    if (this.total >= 1) {
        this.tail[this.total - 1] = createVector(this.x, this.y);
      }

    this.x = this.x + this.xspeed * scl;
    this.y = this.y + this.yspeed * scl;

    this.x = constrain(this.x, 0, width-scl);
    this.y = constrain(this.y, 0, height - scl);

  }

  show(){ //show the shots
      fill(155);
    for(let i = 0; i<this.total;i++){ //create shots
      fill(random(0,255),random(0,255),random(0,255));
      image(shots,this.tail[i].x, this.tail[i].y+(scl/2), scl,80 );
    }
    image(pitbull,this.x, this.y+(scl/2), scl,80);
  }
}
