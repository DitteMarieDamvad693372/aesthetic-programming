//this code is made from the Daniel Shiffmans codeing challenge about the snake game
//I can't not take credit for most of the code, but I have rewritten it and I do understand it

let f;
let scl = 30;
let fireballs;
let mode=0; //starting mode
let song;




function preload(){
  pitbull = loadImage("pitbull1.png");
  shots = loadImage("shots.png");
  fireball = loadImage("fireball.png");
}



function setup() {
  createCanvas(600,600);
  song = loadSound('song.mp3');

  f = new Shots(); //create new shots
  frameRate(8);
  pickLocation();
}
function mousePressed() {
  if (song.isPlaying()) {
    // .isPlaying() returns a boolean
    song.stop();
    background(255, 0, 0);
  } else {
    song.play();
    background(0, 255, 0);
  }
}
function pickLocation(){ //pick the location where the fireball bottle is going to be placed
  let cols = floor(width/scl);
  let rows = floor(height/scl);

  fireballs = createVector(floor(random(cols)),floor(random(rows)));
  fireballs.mult(scl);
}


function draw() {
  background(random(255),random(255),random(255));

  if (mode==0){ //starting mode --> intro text
    let lines = ('Help Pitbull collect shots of Fireball \n Get as many bottles as possible\nClick canvas for surprise :) \nPress space to start')
    fill(0);
    textSize(25);
    textAlign(CENTER);
    textLeading(30); //space between the lines
    text(lines,295,250)

  } else if(mode==1){ //game mode
    f.death();
    f.update();
    f.show();

    if (f.pick(fireballs)){ //pick up the shots
      pickLocation();
    }

    //create friends
    noStroke();
    fill(155);
    image(fireball,fireballs.x,fireballs.y+(scl/2),scl,70 );

    displayScore(); //show how many shots you have picked up
  }
}

function displayScore() { //function to display how many shots you have picked up
    fill(50, 160);
    textSize(20);
    text('You have picked up '+ f.total + " shot(s)", 300, height-20);
}


function keyPressed() { //function to decide the direction of the snake through pressing different keys
  if (keyCode === 32){ //to change mode
    if(mode==0){
      mode++;
    } else if(mode==1){
      mode=1;
    }
  }

  if (keyCode === UP_ARROW) {
    f.dir(0, -1);
  } else if (keyCode === DOWN_ARROW) {
    f.dir(0, 1);
  } else if (keyCode === RIGHT_ARROW) {
    f.dir(1, 0);
  } else if (keyCode === LEFT_ARROW) {
    f.dir(-1, 0);
  }
}
