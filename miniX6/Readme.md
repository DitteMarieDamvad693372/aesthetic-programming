## MiniX6 - Object abstraction


[Click here to run my MiniX6](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX6/)

[Click here for my code](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX6/sketch.js)

[Click here for my code](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX6/fireball.js)

![Picture of my program](/miniX6/miniX62.png "Picture of my program")

![Picture of my program](/miniX6/miniX61.png "Picture of my program")





## My program

In the beginning, I had many ideas about the type of game I wanted to make. After some time, however, I discovered that I had difficulty programming any game and therefore searched for inspiration online. I watched Daniel Shiffmans videos and stumbled across his 'Snake game'(https://thecodingtrain.com/CodingChallenges/115-snake-game-redux.html).  I wanted to copy that and put my own spin on it, making it more fun.  

Here, I wanted to add a homepage where you are guided on how to play the game and how to start.   When you finished the game, you should have been able to restart it by pressing a button but I wasn't able to that.

For my sixth miniX I have made a game similar to the original nostalgic 'snake game' with a fun twist. In my program, Pitbull the singer/rapper is the snake hunting a bottle of fireball. When he obatins a bottle, a shot is automatically added as a 'train' that follows him. When you press the canvas his iconic son "Fireball" plays, and the background flashes random colors as if in a club. 


## Objects and their related attributes

I implemented a class for the purpose of encapsulating the idea of data and functionality into an object, in this case the snake, since it's an object-oriented program.To control the way the game goes, I have also implemented various functions, such as function foodLocation and function Keypressed.


## Object-oriented programming (OOP) and the wider implications of abstraction

Snake, as well as the tofu game we were presented in class, is considered object-oriented programming because the game is organized and concrete even though objects are abstracted. Furthermore, the book (aesthetic programming) mentions how object-oriented programming is intended to reflect the way things are organized and imagined, at least from a computer programmer's perspective. By examining how relatively independent objects interact with each other, it provides an understanding of how they function.


OOP is seen as concrete, even though objects are abstractions, as previously mentioned. OOO, however, is object-oriented ontology, in which the existence and interactivity of objects are analyzed philosophically.There is a fundamental difference between these two methods: OOO rejects the idea that objects are created by perceptions of humans, and it promotes the idea that objects, whether human or not, are autonomous.


## Cultural context

As a social-technical practice, object-oriented modeling of the world constructs new forms of agency by compressing and abstracting relations operative at different scales of reality. (Page 161).

There is an interplay between abstract and concrete reality created by the abstraction of relations. The Snake game is very hard to compare with this approach, however, you can argue that it is a very abstract snake since the snake is pitbull and his 'train' is shots, when he collects fireballs. 

In the book Soon & Cox states : "Abstraction is one of the key concepts of “Object-Oriented Programming” (OOP), a paradigm of programming in which programs are organized around data, or objects, rather than functions and logic" (soon & Cox p. 145). 
We can see how in OOP abstraction is used to refer to and decide what data and how it's used, and how it's not just a question of whether one function draws on a particular piece of data, but rather how it's stored in objects and how it can be used.

An important consideration when talking about abstraction is how some details will be ignored when converting a real-world object into a computer program (Soon & Cox, p. 145). The same thing can be seen in my program, where people are "reduced to" (if you can call it that) only having a few properties that are not true to the characteristics of real people. A real human can perform a multitude of different actions, whereas they can only perform a few. It is also associated with the following quote: "With the abstraction of relations, there is an interplay between abstract and concrete reality." (Soon & Cox, p. 160), in which it can be seen how you can in your programs build up and decide how different objects interact, which is sometimes different from how they interact in the real world.
