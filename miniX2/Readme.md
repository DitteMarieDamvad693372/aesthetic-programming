## MiniX2 🍆🍑


[Click here to run my MiniX2](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX2/)

_Below is a screenshot of my MiniX_


![Picture of my program](/miniX2/miniX2.png "Picture of my program")

# Down to the Basic's 

For my second minix I decided to keep it short and sweet. My focus was to fulfill the task without using any other fancy code that would obstruct me from fully consuming myself in the basics. 

The task was to Explore shapes, geometries, and other related syntax (via p5.js references) and design two emojis. I did that by using the ellipse () function and played around with where they should be placed on the x and y axis. Here I would say I learned a lot by trial and error and ended up with the end result that I had anticipated from the start. I ended up creating text to create a greater sense of provocation and understanding of my emojis. 
By keeping it simple and to the task I now feel a full sense of understanding how placement, text and shapes work when coding. 

My miniX2 illustrates the eggplant emoji next to the peach emoji, created by ellipses in different shapes and sizes. At the top of the canvas there is a text saying, “How do you identify?”, and at the bottom of the eggplant it says “Vegetable”, while it says “Fruit” at the bottom of the peach.



## Meaning

Before I started writing my code, I knew I wanted to create something that would provoke a certain feeling and understanding of gender, based on how our society views it. Growing up in a society using emojis for various things, we (collectively) have asserted different meanings to already somewhat meaningful emojis. In relation to my miniX I want to especially draw attention to how we view the eggplant and peach emoji. I assume that because of a lack of representation of female and male genitalia, society have collectively agreed to associate the male genitalia to an eggplant and the female genitalia to a peach. Although I through research discovered that the peach emoji is seen more as a bum, than as female genitalia. I also discovered that eggplants aren’t even vegetables but fruit: 

>>>

Contrary to what most people believe, eggplants are technically fruits, not vegetables. After planting, they grow as flowering plants that contain seeds, unlike most vegetables. Depending on where you are in the world, eggplants appear in different shapes, sizes, and colors in different parts of the globe.

>>>

So with that in mind together with the previous information, my miniX doesn’t even make sense. But for the sake of my original thought, please ignore it :-)

My idea with my illustration is to add to the conversation around what gender even is, by provoking people to identify as a fruit or vegetable. Also as a way to make you think about the stupid annotations we assign seemingly innocent emojis to the extent that we refrain from using them, afraid of what the other person we are texting will associate them with. 





## References


[What do the aubergine and peach emoji mean?, METRO](https://metro.co.uk/2018/01/18/what-do-the-aubergine-and-peach-emoji-mean-7240646/)

[ Eggplant Emoji: The Sexual and Wholesome Uses of the Purple Aubergine, Emoji guide (Emoji quote used in text)](https://emojiguide.com/blog/eggplant-emoji/)

[P5.js Text()](https://p5js.org/reference/#/p5/text)

[P5.js ellipse()](https://p5js.org/reference/#/p5/ellipse)





