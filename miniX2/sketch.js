function setup() {
  createCanvas(1150, 800);
}

function draw() {
background(220);

noStroke();
fill(10)
textSize(50);
textAlign(LEFT)
text('VEGETABLE',65,600, width);

noStroke();
fill(10)
textSize(50);
textAlign(CENTER)
text('FRUIT',308,600,width);

noStroke();
fill(50)
textSize(50);
textAlign(LEFT)
text('HOW DO YOU IDENTIFY?',250,100, width);


fill(72, 50, 72)
noStroke()
ellipse(200,414,120,350)

fill(0,128,0)
ellipse(200, 262,12,45)
ellipse(178, 270,12,45)
ellipse(222, 268,12,45)
ellipse(200, 253,65,25)
ellipse(200, 240,15,45)

fill(255,255,255)
noStroke()
ellipse(227,340,10,40)

fill(0,128,0)
noStroke()
ellipse(845, 290,75,25)
ellipse(920, 290,75,25)

fill(249, 139, 136)
noStroke();
ellipse(900,400,195,220)
ellipse(860,400,195,220)

noFill();
stroke(155,103,60)
strokeWeight(8)
line(885,265,885,292)

noStroke();
fill(222, 93, 130,30)
ellipse(860,400,180,220)

}
