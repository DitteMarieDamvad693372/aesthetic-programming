## MiniX3 


[Click here to run my MiniX3](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX3/)


[Click here for my code](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX3/sketch.js)


![Picture of my program](/miniX3/throbber.gif "Picture of my program")


## Redesigning an “animated” throbber.

!DISCLAIMER! I AM AWARE THAT THE MOON AND SUN DOES NOT ROTATE AROUND THE EARTH

Describe your throbber design, both conceptually and technically.

For my third MiniX I wanted to explore the 'normal' throbber and experiment with ways of altering it, so it might alter the meaning of time and what lies behind what the machine is actually doing. I started with the 'normal' throbber presented in the Aestethic Porgramming book under 3.3 Source code. Here I just copied the code, and experimented with altering different parts. I then ended up adding two rotating ellipsed around a static ellipse in the middle. I used the for() syntax to make a loop of the three dots after my text. I used them this way simply because I coudld'nt find another way to use them. My final product is an earth in the middle of a blue canvas with white clouads sorrounding it, and a moon + a sun rotating around the earth. 


I had an idea to illustrate time passing in a more visual way, as a way to make it more understadable and tangible. I created a sun and a moon that rotates around the earth in the middle, hereby 'preserving' the original undersandting/illustration of a throbber. The sun rotates forwards and the moon rotates backwards, to create a more interesting throbber. I AM AWARE THAT THE MOON AND SUN DOES NOT ROTATE AROUND THE EARTH, but I thought this would be more aestethically pleasing AND it kinda adds a new meaning to it, as it becomes an illustartion of our selfish view on how everything revolves around us, referring to the term 'Solipsism'. 

When thinking og an assigned meaning to the throbber I toyed with the idea of a year passing everytime the earth had rotated fully around the sun. This is universal knowlegde, and therefore almost every person who views my throbber will associate it with a year passing. My throbber therefore illustrates that a long time is passing, and I wanted it to illustrate how much time we use online, with my throbber being a reminder of how time is passing outside of the screen. When encountering a throbber in everyday digital culture, e.g. when streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction I think a throbber communicates a waiting time that we know is'nt too long, as we have learned through the years of encountering it, that we just have to wait a bit at least at most times. It also really doesnt communicate anything other than a problem has occurred or something is or needs to be loaded. I would argue that my throbber does the same, but implies that a lot more time is passing, and as I said kind of provokes a thougt process of exactly HOW much time you spend. With the text under the earth saying "LOADING WORLD..." I also wanted to create a thought process of what is happening in the world, while you are waiting at the loading screen. 



[Britannica, Solipsism ](https://www.britannica.com/topic/solipsism)

[P5.js for()](https://p5js.org/reference/#/p5/for)

[ Soon, Winnie & Cox, Jeff, Aesthetic Programming, 3. Infinite Loops - 3.3 Source code (Used to create the throbber)](https://aesthetic-programming.net/pages/3-infinite-loops.html#minix-designing-a-throbber-27866)
