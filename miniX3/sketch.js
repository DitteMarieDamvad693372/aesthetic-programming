function setup() {
 //create a drawing canvas
createCanvas(windowWidth, windowHeight);

 frameRate (8);  //the speed of the sun
}

function draw() {
  background(25,217,253);
  drawElements();

  //earth
  noStroke()
  fill(	0, 0, 128)
  ellipse(710,420,200,200)
  noStroke()
  fill(0,102,0)
  ellipse(770,422,80,60)
  noStroke()
  fill(0,102,0)
  ellipse(787,440,40,50)
  noStroke()
  fill(0,102,0)
  ellipse(680,380,70,30)
  noStroke()
  fill(0,102,0)
  ellipse(665,380,70,20)
  noStroke()
  fill(0,102,0)
  ellipse(670,460,60,80)
  noStroke()
  fill(0,102,0)
  ellipse(675,460,60,80)
  noStroke()
  fill(0,102,0)
  ellipse(675,470,60,80)
  noStroke()
  fill(0,102,0)
  ellipse(700,470,80,60)
  noStroke()
  fill(0,102,0)
  ellipse(740,350,20,10)

  //clouds
  noStroke()
  fill(255)
  ellipse(150,260,60, 40)
  ellipse(175,260,65, 40)
  ellipse(190,266,60, 50)
  ellipse(190,240,60, 50)
  ellipse(220,240,50, 55)
  fill(255);
  fill(255)
  ellipse(1200,200,80,55)
  fill(255)
  ellipse(1250,212,80,55)
  fill(255)
  ellipse(1255,190,80,55)
  fill(255)
  ellipse(790,10,80,55)
  fill(255)
  ellipse(750,10,90,65)
  fill(255)
  ellipse(166,10,70,30)
  fill(255)
  ellipse(180,10,70,50)
  fill(255)
  ellipse(210,10,70,50)
  fill(255)
  ellipse(150,680,70,60)
  fill(255)
  ellipse(160,710,70,80)
  fill(255)
  ellipse(190,690,70,75)
  fill(255)
  ellipse(210,700,70,60)
  fill(255)
  ellipse(600,830,80,60)
  fill(255)
  ellipse(630,830,80,65)
  fill(255)
  ellipse(1300,830,80,60)
  fill(255)
  ellipse(1330,810,95,65)
  fill(255)
  ellipse(1380,810,95,65)
  fill(255)
  ellipse(1380,810,95,65)
  fill(255)
  ellipse(1380,500,65,75)
  fill(255)
  ellipse(1360,500,85,65)
  fill(255)
  ellipse(1340,480,85,65)
  fill(255)
  ellipse(1320,500,85,65)
  fill(255)
  ellipse(1310,520,85,60)
  fill(255)
  ellipse(1390,10,85,60)
  fill(255)
  ellipse(1370,10,75,45)

  // Text shadow
  fill(78,77,70)
  textSize(25)
  textStyle(BOLD)
  text("LOADING WORLD",568,570)

  // Main text
  fill(255)
  textSize(25)
  textStyle(BOLD)
  text("LOADING WORLD",565,570)

  // for-loop for the dots/periods shade
  for(let t = 0; t < 60; t++){
    fill(78,77,70);
    ellipse(803 + t, 565, 10);
    t += 20
}
    // for-loop for the dots/periods main
  for(let i = 0; i < 60; i++){
      fill(255);
      ellipse(800 + i, 565, 10);
      i += 20
  }

}

  function drawElements() {
  let num = 20;

  push();
  //move things to the center
  translate(width/2, height/2);
  /* 360/num >> degree of each ellipse's movement;
  frameCount%num >> get the remainder that to know which one
  among 8 possible positions.*/
  let sun = 360/num*(frameCount%num);
  rotate(radians(sun));
  noStroke();

  //sun
  fill(255,216,1)
  ellipse(300,350,90,90);

  pop();

  push();
  //move things to the center
  translate(width/2, height/2);
  /* 360/num >> degree of ellipse movement;
  frameCount%num >> get the remainder to know which one
  among 8 possible positions.*/
  let moon = -360/num*(frameCount%num);
  rotate(radians(moon));
  noStroke();

  //moon
  fill(194,197,204)
  ellipse(200,250,90,90);
  fill(164,165,167,50)
  ellipse(200,279,22,15);
  fill(164,165,167,50)
  ellipse(200,289,35,10);
  fill(164,165,167,50)
  ellipse(195,260,70,60);
  fill(164,165,167,50)
  ellipse(195,220,29,18);
  fill(164,165,167,50)
  ellipse(210,225,42,28);
  pop();

}
