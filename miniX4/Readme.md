## MINIX 4 - Capture All

[Click here to run my MiniX4](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX4/)


[Click here for my code](https://dittemariedamvad693372.gitlab.io/aesthetic-programming/miniX4/sketch.js)

![Picture of my program](/miniX4/atwhatcost.png "Picture of my program")

# AT WHAT COST? By Ditte Marie Damvad

_Have you ever wondered just how much your data is worth?
In a world where everything is starting to become capitalized and our digital footprint is starting to be worth more than we know for big cooperations, just how much of our privacy, is being financed?_


AT WHAT COST provokes with a look into a possible future where you might be able to buy tangible things for access to your personal data. It hopefully evokes some deep feelings about the internet and what you choose to accept, when it comes to sharing YOUR data. 


# My program

For my fourth miniX I decided to make another thought provoking program. It contains the words “What do you want to buy? Click to see the price”, with four buttons beneath it with different options of things to by, for example a T-shirt. When you click the buttons, an exaggerated price of how much data you must surrender, to buy that item. 
For my program I used the data capture inputs mouse and buttons. For the first time I used the mousepressed() and createButton() syntax. They were honestly quite easy to operate, and I am a bit disappointed in myself that I didn’t explore more syntaxes, but here we are. During the process of making the program, I learned that you are able to just copy and paste emojis into the code, and it would show up. 
I chose to go simple because the task overwhelmed me a little, since “shut up and code” was cancelled. Initially I was working on a more complicated program, but found I needed help to execute it fully (easy to say I know). I will definitely explore some more and new syntax for the next minix assignment.

# Capture all

My program addresses the theme “Capture all” by forcing the viewer to think about the theme of the capitalization of data. I wanted to highlight how much data is worth, and therefore how you should be mindful of what you are agreeing too, when using the internet. Before reading about what data is kept, sold and used, I must admit that I didn’t really care that my data was being kept. When growing up in a digital age, I would argue that some people at my age and younger don’t really question things like cookies and terms & conditions agreements etc. We just agree to the terms, to access what we want on the internet, without further thinking of what we have just agreed upon. However, with the increasing focus on privacy and the need for transparency on the internet, young people like myself are taught and have access to various forms of critical sources on data capture, and what our data is being used for. 
From my point of view, I think where things started to click for me, was when I realized that my data were capitalized upon and used to predict future patterns and lure me into buying things, based on my data history. It really put things into perspective for me, and I am now more mindful when browsing the internet. Though, I think the question is whether this capitalization of data is pure evil spun by the bigger cooperations or if it’s just a part of how the future of the internet is developing and adapting? 


# References

[P5.js createButton() ](https://p5js.org/reference/#/p5/createButton)

[P5.js mousePressed()](https://p5js.org/reference/#/p5.Element/mousePressed)

[P5.js text()](https://p5js.org/reference/#/p5/text)


