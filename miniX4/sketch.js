function setup() {
createCanvas(windowWidth, windowHeight);
background('lightblue');
}

function draw() {
  fill('white');
  textSize(55);
  textStyle(BOLD);
   text('What do you want to buy?', 380, 90);
   fill('black')
   text('What do you want to buy?', 384, 90);
   fill('white');
   textSize(17);
   textStyle(ITALIC);
   text('Click to see the price', 625, 130);

//Now im creating the buttons with the items written on them

  let col = color('white');
  let button1 = createButton('A house 🏠 ');
  button1.style('background-color', col);

  button1.size(100,100)
  button1.position(200, 500);
  button1.mousePressed(medical);

  let button2 = createButton('One cup of coffee ☕ ');
  button2.style('background-color', col);
  button2.size(100,100)
  button2.position(480, 300);
  button2.mousePressed(cookies);

  let button3 = createButton('A new t-shirt 👕');
  button3.style('background-color', col);
  button3.size(100,100)
  button3.position(750, 500);
  button3.mousePressed(lokation);

  let button4 = createButton('A porsche 🏎️ ');
  button4.style('background-color', col);
  button4.size(100,100)
  button4.position(1020, 300);
  button4.mousePressed(soul);

}

// The following functions is what appears, when the buttons are pressed. 
function medical() {
  fill('yellow');
  textSize(20);
  textStyle(BOLD);
  text('💸 PRICE: Give access to all medical files 💸', 55, 450);
}

function cookies(){
  fill('yellow');
  textSize(20);
  textStyle(BOLD);
  text(' 💸 PRICE: Accept cookies on a given site 💸', 320, 250);

}

function lokation(){
  fill('yellow');
  textSize(20);
  textStyle(BOLD);
  text(' 💸 PRICE: Allow location monitoring for 24 hours 💸', 555, 450);

}

function soul(){
  fill('yellow');
  textSize(20);
  textStyle(BOLD);
  text(' 💸 PRICE: Your soul 💸', 950, 250);

}
